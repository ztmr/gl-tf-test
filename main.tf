provider "local" { version = "~> 1.4" }

resource "local_file" "test" {
    content  = "testing-content"
    filename = "stuff.txt"
}
